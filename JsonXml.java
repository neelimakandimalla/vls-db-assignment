package com.java.internship.jsontoxmlconvertion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Scanner;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.json.JSONException;   
import org.json.JSONObject;
import org.json.XML;

public class JsonXml {

	public static void main(String[] args) {
		
        String finalStr = "";
        String finalXml = "";
        String xml = "";
        
        finalStr = readingTxtFile();
       
        xml = converter(finalStr, "root");
        
        finalXml = formattingXml(xml);
        
        writingTxtFile(finalXml);

	}
	
    private static boolean writingTxtFile(String finalXml) {
		
    	try{    
            FileWriter fw=new FileWriter("E:\\\\Lucky Assignments\\\\news_feedsXml.txt");    
            fw.write(finalXml);    
            fw.close();  
            return true;
        } catch(Exception e){
        	System.out.println(e);
        }
		
    	return false;
	}

	private static String formattingXml(String xml) {
		
    	 try {
             Source xmlInput = new StreamSource(new StringReader(xml));
             StringWriter stringWriter = new StringWriter();
             StreamResult xmlOutput = new StreamResult(stringWriter);
             TransformerFactory transformerFactory = TransformerFactory.newInstance();
             transformerFactory.setAttribute("indent-number", "2");
             Transformer transformer = transformerFactory.newTransformer(); 
             transformer.setOutputProperty(OutputKeys.INDENT, "yes");
             transformer.transform(xmlInput, xmlOutput);
             xml = xmlOutput.getWriter().toString();
         
         } catch (Exception e) {
             throw new RuntimeException(e); 
         } 
		
    	 return xml;
	}

	private static String readingTxtFile() {
		
    	 File file = new File("E:\\Lucky Assignments\\news_feeds.txt");
         Scanner scanner;
         String resultString = "";
 		
         try {
 			scanner = new Scanner(file);
 			while(scanner.hasNextLine()) {
 	        	String str = scanner.nextLine();
 	        	resultString = resultString + str;
 	        }
 	        scanner.close();
 		} catch (FileNotFoundException e1) {
 			e1.printStackTrace();
 		}
         
         return resultString;
		
	}

	public static String converter(String json, String root) {
        
        String xml = "";

        try {

            JSONObject jsonObject = new JSONObject(json);

            xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-15\"?>\n<"+root+">" + XML.toString(jsonObject) + "</"+root+">"; 

        } catch(JSONException ex) {
            System.out.println(ex.getMessage());
        }
    
        return xml;
    }


}
